package com.androidcodefinder.loginapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class WelcomeScreen extends AppCompatActivity {

    ImageView petugas, warga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);

        petugas = (ImageView) findViewById(R.id.petugas);
        warga = (ImageView) findViewById(R.id.warga);

        warga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent(WelcomeScreen.this, LoginWarga.class);
                finish();
                startActivity(it);
            }
        });

        petugas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent(WelcomeScreen.this, LoginPetugas.class);
                finish();
                startActivity(it);
            }
        });

    }

}
